
#ifndef _SPACEHUNT_H             // prevent multiple definitions if this 
#define _SPACEHUNT_H             // ..file is included in more than one place
#define WIN32_LEAN_AND_MEAN


#include <string> 
#include "game.h"
#include "textureManager.h"
#include "image.h"

namespace spacehuntNS
{
	const char FONT[] = "Arial Bold"; //font
	const int FONT_BIG_SIZE = 256; // font height
	const int FONT_SCORE_SIZE = 48;
	const int BUF_SIZE = 20;
	const int SCORE_Y = 10;
	const int SCORE_X = 40;
}

//=============================================================================
// Create game class
//=============================================================================
class Spacehunt : public Game
{
private:
	// game items
	TextureManager spacehuntTexture;   // spacehuntbackground texture
	TextureManager playerTexture;   // player texture
	TextureManager asteroidTexture; // asteorid texture
	TextureManager alienTexture; // alien texture
	TextureManager treasureTexture; // treasure box texture
	TextureManager gemTexture; // gemstone texture
	TextureManager hudTexture; // hud texture
	Image   player;                 // player image
	Image	asteroid;				// asteroid image
	Image   alien;                 // alien image
	TextDX score;  //score text
	Image	treasurebox;				// treasure box image
	Image   gemstone;                 // gemstone image
	Image   spacehunt;                 // spacehuntbackground image
	Image hud;						//hud image
	char buffer[spacehuntNS::BUF_SIZE];
	int shipscore;
public:
	// Constructor
	Spacehunt();

	// Destructor
	virtual ~Spacehunt();

	// Initialize the game
	void initialize(HWND hwnd);
	void update();      // must override pure virtual from Game
	void ai();          // "
	void collisions();  // "
	void render();      // "
	void releaseAll();
	void resetAll();
	void scoring();
};

#endif

