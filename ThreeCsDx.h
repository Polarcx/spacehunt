#ifndef _THREECS_H
#define _THREECS_H
#define WIN32_LEAN_AND_MEAN

#include "game.h"
#include "textDX.h"
#include "constants.h"


class ThreeCsDX : public Game
{
private:
	//game items
	TextDX *dxFontSmall; //directx fonts
	TextDX *dxFontMedium;
	TextDX *dxFontLarge;
public:
	ThreeCsDX();
	virtual ~ThreeCsDX();
	virtual void initialize(HWND hwnd);
	virtual void releaseAll();
	virtual void resetAll();
	virtual void render();

};

ThreeCsDX::ThreeCsDX()
{
	dxFontSmall = new TextDX();     // DirectX fonts
	dxFontMedium = new TextDX();
	dxFontLarge = new TextDX();
}


ThreeCsDX::~ThreeCsDX()
{
	releaseAll();          // call deviceLost() for every graphics item
	SAFE_DELETE(dxFontSmall);
	SAFE_DELETE(dxFontMedium);
	SAFE_DELETE(dxFontLarge);
}
#endif