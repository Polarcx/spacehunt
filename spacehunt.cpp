#include "spacehunt.h"

//=============================================================================
// Constructor
//=============================================================================
Spacehunt::Spacehunt()
{
	shipscore = 0;
}


//=============================================================================
// Destructor
//=============================================================================
Spacehunt::~Spacehunt()
{
	releaseAll();           // call onLostDevice() for every graphics item
}

//=============================================================================
// Initializes the game
// Throws GameError on error
//=============================================================================
void Spacehunt::initialize(HWND hwnd)
{
	Game::initialize(hwnd); // throws GameError

	score.initialize(graphics, spacehuntNS::FONT_SCORE_SIZE, false, false, spacehuntNS::FONT);
							// Game background texture
	if (!spacehuntTexture.initialize(graphics, SPACE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing space background texture"));

	// Player Sprite texture
	if (!playerTexture.initialize(graphics, PLAYER_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player sprite texture"));

	// Asteroid Sprite texture
	if (!asteroidTexture.initialize(graphics, ASTEROID_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing asteroid sprite texture"));

	// Alien Sprite texture
	if (!alienTexture.initialize(graphics, ALIEN_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing alien sprite texture"));

	// Treasure Box Sprite texture
	if (!treasureTexture.initialize(graphics, TREASURE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing treasure box sprite texture"));

	// Gemstone Sprite texture
	if (!gemTexture.initialize(graphics, GEMSTONE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing gemstone sprite texture"));

	//hud texture
	if (!hudTexture.initialize(graphics, HUD_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing hud texture"));

	// Game background
	if (!spacehunt.initialize(graphics, 0, 0, 0, &spacehuntTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing space background"));

	// Player Sprite
	if (!player.initialize(graphics, 0, 0, 0, &playerTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player sprite"));
	// place player sprite to the starting point (left) of screen
	player.setX(GAME_WIDTH*0.05f - player.getWidth()*0.05f);
	player.setY(GAME_HEIGHT*0.45f - player.getHeight()*0.45f);

	// Asteroid Sprite
	if (!asteroid.initialize(graphics, 0, 0, 0, &asteroidTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing asteroid sprite"));
	asteroid.setX(GAME_WIDTH*0.7f - asteroid.getWidth()*0.7f);
	asteroid.setY(GAME_HEIGHT*0.8f - asteroid.getHeight()*0.8f);

	// Alien Sprite
	if (!alien.initialize(graphics, 0, 0, 0, &alienTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing alien sprite"));
	alien.setX(GAME_WIDTH*0.9f - alien.getWidth()*0.9f);
	alien.setY(GAME_HEIGHT*0.2f - alien.getHeight()*0.2f);

	// Treasure Box Sprite
	if (!treasurebox.initialize(graphics, 0, 0, 0, &treasureTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing treasure box sprite"));
	treasurebox.setX(GAME_WIDTH*0.45f - treasurebox.getWidth()*0.45f);
	treasurebox.setY(GAME_HEIGHT*0.15f - treasurebox.getHeight()*0.15f);

	// Gemstone Sprite
	if (!gemstone.initialize(graphics, 0, 0, 0, &gemTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing gemstone sprite"));
	gemstone.setY(GAME_HEIGHT*0.65f - gemstone.getHeight()*0.65f);

	//hud sprite
	if (!hud.initialize(graphics, 0, 0, 0, &hudTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing hud sprite"));
	hud.setX(GAME_WIDTH*0.45f - hud.getWidth()*0.45f);
	hud.setY(GAME_HEIGHT*0.30f - hud.getHeight()*0.30f);

	return;
}

//=============================================================================
// Update all game items
//=============================================================================
void Spacehunt::update()
{	
}

//=============================================================================
// Artificial Intelligence
//=============================================================================
void Spacehunt::ai()
{}

void Spacehunt::scoring()
{
	if (GetKeyState(VK_DOWN) & 0x28)
	{
		shipscore+1;
	}
}

//=============================================================================
// Handle collisions
//=============================================================================
void Spacehunt::collisions()
{}

//=============================================================================
// Render game items
//=============================================================================
void Spacehunt::render()
{
	graphics->spriteBegin();                // begin drawing sprites
	spacehunt.draw();                          // add the space background to the scene
	player.draw();                          // add the player sprite to the scene
	asteroid.draw();						// add the asteroid sprite to the scene
	alien.draw();						// add the alien sprite to the scene
	gemstone.draw();						// add the gemstone sprite to the scene
	treasurebox.draw();						// add the treasure box sprite to the scene
	hud.draw();  // draw hud
	_snprintf_s(buffer, spacehuntNS::BUF_SIZE , "%d" , shipscore);
	score.print(buffer, spacehuntNS::SCORE_X, spacehuntNS::SCORE_Y);

	
	
	graphics->spriteEnd();                  // end drawing sprites
}



//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void Spacehunt::releaseAll()
{
	playerTexture.onLostDevice();
	spacehuntTexture.onLostDevice();
	asteroidTexture.onLostDevice();
	alienTexture.onLostDevice();
	treasureTexture.onLostDevice();
	gemTexture.onLostDevice();
	hudTexture.onLostDevice();
	score.onLostDevice();
	Game::releaseAll();
	return;
}

//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void Spacehunt::resetAll()
{
	spacehuntTexture.onResetDevice();
	playerTexture.onResetDevice();
	asteroidTexture.onResetDevice();
	alienTexture.onResetDevice();
	treasureTexture.onResetDevice();
	gemTexture.onResetDevice();
	hudTexture.onResetDevice();
	score.onResetDevice();

	Game::resetAll();
	return;
}
