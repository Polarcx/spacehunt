
#ifndef _CONSTANTS_H            // prevent multiple definitions if this 
#define _CONSTANTS_H            // ..file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include <windows.h>

//-----------------------------------------------
// Useful macros
//-----------------------------------------------
// Safely delete pointer referenced item
#define SAFE_DELETE(ptr)       { if (ptr) { delete (ptr); (ptr)=NULL; } }
// Safely release pointer referenced item
#define SAFE_RELEASE(ptr)      { if(ptr) { (ptr)->Release(); (ptr)=NULL; } }
// Safely delete pointer referenced array
#define SAFE_DELETE_ARRAY(ptr) { if(ptr) { delete [](ptr); (ptr)=NULL; } }

// Safely call onLostDevice
#define SAFE_ON_LOST_DEVICE(ptr)    { if(ptr) { ptr->onLostDevice(); } }
// Safely call onResetDevice
#define SAFE_ON_RESET_DEVICE(ptr)   { if(ptr) { ptr->onResetDevice(); } }
#define TRANSCOLOR  SETCOLOR_ARGB(0,255,0,255)  // transparent color (magenta)

//-----------------------------------------------
//                  Constants
//-----------------------------------------------
// window
const char CLASS_NAME[] = "Space Hunt";
const char GAME_TITLE[] = "Space Hunt";
const bool FULLSCREEN = false;              // windowed or fullscreen
const UINT GAME_WIDTH =  960;               // width of game in pixels
const UINT GAME_HEIGHT = 540;               // height of game in pixels
 
// game
const double PI = 3.14159265;
const float FRAME_RATE  = 200.0f;               // the target frame rate (frames/sec)
const float MIN_FRAME_RATE = 10.0f;             // the minimum frame rate
const float MIN_FRAME_TIME = 1.0f/FRAME_RATE;   // minimum desired time for 1 frame
const float MAX_FRAME_TIME = 1.0f/MIN_FRAME_RATE; // maximum time used in calculations

// key mappings
// In this game simple constants are used for key mappings. If variables were used
// it would be possible to save and restore key mappings from a data file.
const UCHAR ESC_KEY      = VK_ESCAPE;   // escape key
const UCHAR ALT_KEY      = VK_MENU;     // Alt key
const UCHAR ENTER_KEY    = VK_RETURN;   // Enter key

// graphic images
const char SPACE_IMAGE[] = "pictures\\spacehuntbg.JPG";  // background source from opengameart.org/content/perfectly-seamless-night-sky
const char PLAYER_IMAGE[] = "pictures\\playersprite.png"; // player spaceship source from clipartmax.com/max/m2i8G6G6N4i8m2N4
const char ASTEROID_IMAGE[] = "pictures\\asteroidsprite.png"; // asteroid enemy sprite source from melbournechapter.net/explore/asteroid-clipart-kuiper-belt/
const char ALIEN_IMAGE[] = "pictures\\alien.png"; // alien enemy sprite source from mbtskoudsalg.com/explore/alien-clipart-sci-fi/
const char TREASURE_IMAGE[] = "pictures\\treasurebox.png"; // treasure box sprite source from kisspng.com/png-treasure-box-cartoon-vintage-wooden-box-318742/
const char GEMSTONE_IMAGE[] = "pictures\\gemstones.png"; // gemstone sprite source from kisspng.com/png-emerald-gemstone-clip-art-emerald-753089/
const char SHIP_IMAGE[] = "pictures\\ship.png";   // spaceship
const char HUD_IMAGE[] = "pictures\\hud.png"; // hud image


#endif
