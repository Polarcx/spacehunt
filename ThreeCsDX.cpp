#include "ThreeCsDx.h"


void ThreeCsDX::initialize(HWND hwnd)
{
	Game::initialize(hwnd);
	graphics->setBackColor(graphicsNS::WHITE);

	// initialize DirectX fonts
	// 15 pixel high Arial
	if (dxFontSmall->initialize(graphics, 15, true, false, "Arial") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));

	// 62 pixel high Arial
	if (dxFontMedium->initialize(graphics, 62, true, false, "Arial") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));
	// 124 pixel high Arial
	if (dxFontLarge->initialize(graphics, 124, true, false, "Arial") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));

	return;

}

void ThreeCsDX::releaseAll()
{
	dxFontSmall->onLostDevice();
	dxFontMedium->onLostDevice();
	dxFontLarge->onLostDevice();
	Game::releaseAll();
	return;
}

void ThreeCsDX::resetAll()
{
	dxFontSmall->onResetDevice();
	dxFontMedium->onResetDevice();
	dxFontLarge->onResetDevice();
	Game::resetAll();
	return;
}

void ThreeCsDX::render()
{
	graphics->spriteBegin();
	dxFontSmall->setFontColor(graphicsNS::BLACK);
	dxFontMedium->setFontColor(graphicsNS::BLACK);
	dxFontLarge->setFontColor(graphicsNS::BLACK);
	dxFontLarge->print("C", 20, 100);
	dxFontMedium->print("C", 114, 148);
	dxFontSmall->print("C", 164, 184);
	graphics->spriteEnd();
}
